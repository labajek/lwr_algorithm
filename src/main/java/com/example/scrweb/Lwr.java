package com.example.scrweb;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Lwr {

    @JsonProperty("task_id")
    private Integer taskId;
    @JsonProperty("machine_id")
    private Integer machineId;
    @JsonProperty("time")
    private Integer timeProcess;
    @JsonProperty("time_arrived")
    private Integer timeArrived;
}
