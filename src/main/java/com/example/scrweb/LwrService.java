package com.example.scrweb;

import com.example.scrweb.domain.Machine;
import com.example.scrweb.domain.Task;

import java.util.List;

public interface LwrService {

    List<Machine> reSequentialTask(List<Machine> machines, List<Task> tasks);

}
