package com.example.scrweb.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Flow {

    private long taskId;
    private long machineId;
    private int capacity;
    private boolean isFinished = false;

    public Flow(long taskId, long machineId, int capacity) {
        this.machineId = machineId;
        this.taskId = taskId;
        this.capacity = capacity;
    }
}
