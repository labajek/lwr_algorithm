package com.example.scrweb.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Optional;

@Getter
@Setter
public class Task implements Comparable<Task> {

    private long id;
    private int startingAt;
    private List<Flow> flow;
    private boolean isFinished = false;
    private boolean isWorking = false;
    private long timeToRelease = -1;
    private boolean isInQueue = false;

    public Task(long id, int startingAt, List<Flow> flow) {
        this.id = id;
        this.startingAt = startingAt + 1;
        this.flow = flow;
    }

    public void removeFlow() {
        this.flow.remove(0);
    }

    public Flow getFirstFlow() {
        return this.getFlow().get(0);
    }

    public Integer getSumToEnd() {
        return flow.stream().map(Flow::getCapacity).reduce(0, Integer::sum);
    }


    @Override
    public int compareTo(Task t) {

        return getSumToEnd().compareTo(t.getSumToEnd());
    }

}
