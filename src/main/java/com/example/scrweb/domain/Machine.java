package com.example.scrweb.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
public class Machine {

    @JsonProperty("machine_id")
    private long id;
    @JsonIgnore
    private Task currentTask;
    @JsonIgnore
    private List<Task> waitingTask = new ArrayList<>();
    private List<Long> workingCapacity = new ArrayList<>();
    @JsonIgnore
    private boolean isTaken;

    public Machine(long id) {
        this.id = id;
    }

    public void addToWorkingCapacity(Long i) {
        this.workingCapacity.add(i);
    }

    public void addWaitingTask(Task task) {
        this.waitingTask.add(task);
    }

    public void removeWaitnigTask() {
        if (this.waitingTask.size() > 1) {
            this.waitingTask.remove(0);
        } else {
            setWaitingTask(new ArrayList<>());
        }
    }
}
