package com.example.scrweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ScrWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScrWebApplication.class, args);
    }

}
