package com.example.scrweb;

import com.example.scrweb.domain.Flow;
import com.example.scrweb.domain.Machine;
import com.example.scrweb.domain.Task;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

@Service
@Slf4j
public class LwrServiceImpl implements LwrService {

    @Override
    public List<Machine> reSequentialTask(List<Machine> machines, List<Task> tasks) {
        int i = 0;
        while (!(tasks.stream().allMatch(Task::isFinished))) {
            int finalI1 = i;

            machines.stream().filter(Machine::isTaken).forEach(m -> {
                if (finalI1 == m.getCurrentTask().getTimeToRelease()) {
                    m.setTaken(false);
                    m.getCurrentTask().setTimeToRelease(-1);
                    m.getCurrentTask().setWorking(false);
                    m.getCurrentTask().removeFlow();
                    if (m.getCurrentTask().getFlow().isEmpty()) {
                        m.getCurrentTask().setFinished(true);
                    }
                    m.setCurrentTask(null);
                }
            });

            List<Task> availableTasks = getAvailableTasks(tasks, i);
            List<Flow> flows = availableTasks.stream().map(t -> t.getFlow().stream().findFirst().get()).collect(Collectors.toList());
            List<Long> machinesId = flows.stream().map(Flow::getMachineId).collect(Collectors.toList());

            if (isConflict(flows)) { //and flows != empty
                machines.stream().filter(m -> machinesId.contains(m.getId())).forEach(m -> {
                    List<Flow> conflictedFlows = flows.stream().filter(f -> f.getMachineId() == m.getId()).collect(Collectors.toList());
                    List<Task> conflictedTasks = availableTasks.stream().filter(t -> conflictedFlows.contains(t.getFirstFlow())).collect(Collectors.toList());

                    if (!m.getWaitingTask().isEmpty()) {
                        conflictedTasks.addAll(m.getWaitingTask());
                    }

                    Set<Task> set = new HashSet<>(conflictedTasks);
                    conflictedTasks.clear();
                    conflictedTasks.addAll(set);
                    Collections.sort(conflictedTasks);
                    m.setWaitingTask(conflictedTasks);
                });
            } else {
                machines.forEach(m -> {
                    availableTasks.stream().filter(t -> t.getFirstFlow().getMachineId() == m.getId()).forEach(t -> {
                        if (!m.getWaitingTask().contains(t)) {
                            m.addWaitingTask(t);
                        }
                    });
                });
            }


            if (!flows.isEmpty()) {
                machines.stream()
                        .filter(m -> machinesId.contains(m.getId()))
                        .forEach(m -> {
                            if (!m.isTaken()) {
                                m.setTaken(true);
                                m.setCurrentTask(m.getWaitingTask().get(0));
                                m.getWaitingTask().get(0).setWorking(true);
                                m.getCurrentTask().setTimeToRelease(finalI1 + m.getCurrentTask().getFirstFlow().getCapacity());
                                m.removeWaitnigTask();
                            }
                        });
            }
            machines.stream().filter(m -> !m.isTaken()).forEach(m -> m.addToWorkingCapacity(0L));
            machines.stream().filter(Machine::isTaken).forEach(m -> m.addToWorkingCapacity(m.getCurrentTask().getId()));

            i++;
        }

        return machines;
    }

    public static List<Task> getAvailableTasks(final List<Task> tasks, int time) {
        return tasks.stream()
                .filter(t -> t.getStartingAt() <= time && !t.isFinished() && !t.isWorking())
                .collect(Collectors.toList());
    }

    public static boolean isConflict(List<Flow> flows) {
        final List<List<Flow>> collect = flows
                .stream()
                .collect(groupingBy(Flow::getMachineId))
                .values()
                .stream()
                .filter(list -> list.size() > 1)
                .collect(Collectors.toList());
        return !collect.isEmpty();
    }
}
