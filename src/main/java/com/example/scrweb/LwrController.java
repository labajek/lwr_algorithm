package com.example.scrweb;

import com.example.scrweb.domain.Flow;
import com.example.scrweb.domain.Machine;
import com.example.scrweb.domain.Task;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.asList;
import static java.util.stream.Collectors.groupingBy;

@Controller
@RequiredArgsConstructor
public class LwrController {

    private final LwrService lwrService;

    @GetMapping("/")
    public String root() {
        return "index";
    }

    @PostMapping("/sendPoints")
    @ResponseBody
    public List<Machine> testLwr(@RequestBody List<Lwr> lwrJson) {

        Map<Integer, List<Lwr>> tasks = lwrJson.stream().collect(groupingBy(Lwr::getTaskId));

        List<Task> prepareTask = new ArrayList<>();

        tasks.forEach((key, value) -> prepareTask.add(new Task(key, getArrivedTime(value, key), getFlows(value, key))));


        List<Machine> machines = asList(new Machine(1), new Machine(2),
                new Machine(3), new Machine(4));
        return lwrService.reSequentialTask(machines, prepareTask);
    }

    private Integer getArrivedTime(List<Lwr> lwrJson, int idTask){
        return lwrJson.stream()
                .filter(lwr -> lwr.getTaskId().equals(idTask))
                .map(Lwr::getTimeArrived)
                .findFirst()
                .orElse(0);
    }

    private List<Flow> getFlows(List<Lwr> lwrJson, int idTask){
        return lwrJson.stream()
                .filter(lwr -> lwr.getTaskId().equals(idTask))
                .map(lwr -> new Flow(idTask, lwr.getMachineId(), lwr.getTimeProcess()))
                .collect(Collectors.toList());
    }

}
